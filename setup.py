from setuptools import setup

setup(
    name='marottehs',
    version='1.0.0',
    packages=['marottehs'],
    url='',
    license='',
    author='Ross Marchant',
    author_email='ross.g.marchant@gmail.com',
    description='Marotte HS processing scripts',
    install_requires=['pandas', 'matplotlib', 'numpy', 'scipy'],
    package_data={'': ['calibration.csv']},
    include_package_data=True,
)

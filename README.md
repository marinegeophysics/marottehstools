# Marotte HS Tools

This repository contains scripts to process Marotte HS current meter data.

## Getting Starting

### Never used Python before?

If you have never used Python before we recommend installing _Anaconda_. It provides a base Python environment and tools for running Python scripts.

We also recommend using an _conda environment_ to run scripts. An environment is a separate Python installation where you can better control which Python packages are installed.

Setup instructions:

1. Download Anaconda and install it
2. Open the _Anaconda Command Prompt_ (Windows) or _Terminal_ (MacOS)
3. Create a new environment with the command (press y to when prompted)

    `conda create -n marottehs python==3.7`
    
4. Switch to the new environment 

    `conda activate marottehs`
    
Note: you must activate this environment whenever you want to run some scripts

### Installing this package

From the _Anaconda Command Prompt_ (Windows) or _Terminal_ (MacOS) enter the following command to install (or update) this package:

    pip install -U git+http://bitbucket.org/marinegeophysics/marottehstools
    
Remember to activate your environment of choice first!

## Running the processing script

There are multiple ways to use the library:

- Installed as a package and run via the command line
- As part of a python script

### Via the command line

1. Open the _Anaconda Command Prompt_ (Windows) or _Terminal_ (MacOS)
2. Activate the environment

    `conda activate marottehs`
    
3. Run the processing script using:

    `python -m marottehs [-h] [-i SOURCE] [-s START] [-e END] [-r RESAMPLE] [-n NAME] [-z] [-d] [-p]`
    
    where the options are (run `python -m marottehs --help` to show):
    
```    
Marotte HS Current Meter Processing

optional arguments:
  -h, --help            show this help message and exit
  -i SOURCE, --input SOURCE
                        Current meter record to process
  -s START, --start-date START
                        Start date of deployment written as "YYYY-MM-DD hh:mm:ss" (include quote marks)
  -e END, --end-date END
                        Start date of deployment written as "YYYY-MM-DD hh:mm:ss" (include quote marks)
  -r RESAMPLE, --resample RESAMPLE
                        Time period to smooth and resample in seconds (default 600)
  -n NAME, --name NAME  Name to append to the output director, e.g. "site_15"
  -z, --est-zero        Estimate the zero point from the deployment. Only use if deployed at site where currents occasionally approach 0, e.g. open ocean tidal zones.
  -d, --est-dates       Estimate the valid deployed dates. Use instead of start and end dates
  -p, --show-plots      Show the plots at the end of processing

Example of script with fixed dates and 5 minute resampling:
python -m marottehs -i "C:\Users\Deployments\record.txt" -s "2019-01-01 00:00:00" -e "2019-01-01 00:00:00" -p -r 300

Example of script with estimated dates, estimated zero point and 10 minute resampling:
python -m marottehs -i "C:\Users\Deployments\record.txt" -z -d -p -r 600

If speeds seem offset, try to use zero point estimation if applicable, as the results are usually more accurate
```

### Via a python script

Simply import `marottehs` and call the `process` function. For example the following will process a file with fixed date range, estimated zero point, smoothing window of 600 seconds, output directory named `Test` and show the plots at the end:

```python
from marottehs.processing import process

data, data_smoothed = process(r"C:\Path\To\The\File\To\Process.txt", 
                      date_range=["2019-01-01 00:00:00", "2019-01-01 00:00:00"],
                      estimate_date_range=False,
                      estimate_zero_point=True,
                      resample=600,
                      output_name="Test",
                      show_plots=True)

```

The variables `data` and `data_smoothed` contain the processed records as `pandas` data frames.

`data` contains the processed record at the original sampling rate:
 
- calibrated accelerometer values, used to calculate tilt angle
- calibrate magnetometer values, used to calculate tilt direction
- tilt vector in world coordinates, x = East, y = North, z = Up
- battery voltage
- temperature (deg Celsius)

`data_smoothed` contains the processed record smoothed and resampled:
 
- tilt vector in world coordinates, x = east, y = north, z = up
- battery voltage
- temperature (deg Celsius)
- tilt angle (rad)
- direction (rad CCW from East)
- heading (deg CW from North)
- speed (m/s)
- speed error estimate (m/s)
 
## Processing output

The script will create a folder structure in the same folder as the source file to store the results. The structure is:

- If `output_name` specified: `output/output_name/SERIAL_NAME`
- else: `output/SERIAL_NAME`

where `SERIAL` and `NAME` are the serial number of the instrument and the name in the record file, respectively.

In this directory will be saved a CSV of the processed data smoothed at the new sampling rate, consisting of:

- timestamp
- tilt vector in world coordinates, x = east, y = north, z = up
- battery voltage
- temperature (deg Celsius)
- tilt angle (rad)
- direction (rad CCW from East)
- heading (deg CW from North)
- speed (m/s)
- speed error estimate (m/s) 

Also there will be plots of 

- accelerometer raw and calibrated
- magnetometer raw and calibrated
- tilt angle (rad)
- speed (m/s)
- heading (deg CW from North)
- battery voltage (V)
- temperature (deg Celsius)
 
## Processing parameters
 
The processing parameters can have a big effect on the overall results. Each is explained below:

### Date range

The date range should be the start and end date and time of deployment, when the instrument is actually in the water. If the data rate is ommitted, the entire record will be used.

### Estimate date range

Enabling estimate date range will override any entered data range.

Usually people store the current meters in a horizontal position, whereas once deployed they normally float in an upright position. This method tries to find the largest continuous range of dates in the middle of the record where the instrument was close to upright, and use them as the date range.

The algorithm looks at the deployment on a day-by-day basis, so this should not be used for short deployments.

### Estimate zero point

The zero point should be periodically calibrated in the lab (see user manual for instructions). It is possible for values in the zero point to drift with temperature or change offset, e.g. if the logger board is not quite inserted properly and is loose. In this case, you can estimate the zero point from the data with this option.

The estimation only works if during the deployment there were many times with close to zero current. For example, a typical ocean deployment.

If there is a consistent offset in the calculated speed values, please try using the zero point estimation to see if it improves the result. You may also try to recalibrate the zero point to confirm.

### Resampling

The resampling period is the timespan over which to smooth and resample the data. 

The data will be smoothed using a gaussian function with standard deviation 1/3 the resampling rate, and window size 3 x the sampling rate, then resampled.

### Output name

Optional name of the folder (under the `output` folder) to store the processed values in.

### Show plots

Enable to show the plots at the end of the script running. When running from the command line, this will pause the script. To end the script press Control-C, or to continue the scrip close all the plot windows.

## Troubleshooting

If you encounter any errors or bugs, please [open an issue][1]

[1]: https://bitbucket.org/marinegeophysics/marottehstools/issues?status=new&status=open
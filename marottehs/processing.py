import marottehs.tools as tools
import marottehs.plots as plots
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime


def process(source, date_range=None, estimate_date_range=False, estimate_zero_point=None, resample=None, output_name=None, show_plots=True):

    print("--------------------------------------------------------------------------------------------")
    print("JCU Marine Geophyics Laboratory")
    print("Marotte HS Current Meter Processing")
    print("--------------------------------------------------------------------------------------------")

    # Get the calibration parameters from the file
    print("Filename:    {}".format(source))
    print()
    serial, name, firmware, zero_point, acc_scale, acc_offset, mag_scale, mag_offset = tools.read_calibration(source)
    print("Serial:      {}".format(serial))
    print("Name:        {}".format(name))
    print("Firmware:    {}".format(firmware))
    print("Zero point:  {}".format(zero_point))
    print("Acc. scale:  {}".format(acc_scale))
    print("Acc. offset: {}".format(acc_offset))
    print("Mag. scale:  {}".format(mag_scale))
    print("Mag. offset: {}".format(mag_offset))
    print()

    # Output directory
    if output_name is None:
        output_dir = os.path.join(os.path.dirname(source), "output", os.path.basename(source)[:-4] + '_' + name)
    else:
        output_dir = os.path.join(os.path.dirname(source), "output", output_name, os.path.basename(source)[:-4] + '_' + name)

    # Adjust thresholds for zero point estimation etc based on model number
    if serial.startswith('B'):
        deviation_threshold = 20
        up_threshold = 3000
        date_threshold = 200
    else:
        deviation_threshold = 5
        up_threshold = 750
        date_threshold = 50

    # Check no zeros in scale, meaning calibration wasn't done properly.
    if np.any(acc_scale == 0) or np.any(mag_scale == 0):
        print("Fatal error, zeros detected in the accelerometer or magnetometer scale values.")
        print("Please recalibrate the instrument and then edit this file with the new scale values.")
        print("These are the first three values of acccal or magcal in the file.")
        sys.exit()

    # Load the data as a pandas data frame
    print("Reading record file... ")
    data = tools.read_record(source)
    print("done")
    print("{} records".format(len(data)))
    print("Start date:  {}".format(data.index[0]))
    print("End date:    {}".format(data.index[-1]))
    print("Duration:    {}".format(data.index[-1] - data.index[0]))

    # Find the sampling interval
    interval = float(np.median(np.diff(data.index))) / 1000000000
    print("Interval:    {}s".format(interval))
    print()

    # Apply date range
    if estimate_date_range is True:
        date_range = tools.estimate_date_range(data, date_threshold)
        print("Estimating date range...".format(date_range))
        print("Start:       {}s".format(date_range[0]))
        print("End:         {}s".format(date_range[1]))
        print()
        data = data.loc[date_range[0]:date_range[1]]
    elif date_range is not None:
        print("Using specified date range...".format(date_range))
        print("Start:       {}s".format(date_range[0]))
        print("End:         {}s".format(date_range[1]))
        print()
        date_range = [np.datetime64(date_range[0]), np.datetime64(date_range[1])]
        data = data.loc[date_range[0]:date_range[1]]

    # Transform accelerometer and magnetometer columns into numpy arrays
    acc = tools.get_vector_from_dataframe(data, 'acc')
    mag = tools.get_vector_from_dataframe(data, 'mag')

    # Check what the zero point from the file looks like
    zero_point_est = tools.estimate_zero_point(acc, deviation_threshold, up_threshold)
    print("Original zero point:  {}".format(zero_point))
    print("Estimated zero point: {}".format(zero_point_est))
    if estimate_zero_point is None:
        use_zero = input("Do you wish to use the estimated zero point (y/n)?")
        if use_zero == 'y' or use_zero == 'Y':
            zero_point = zero_point_est
    elif estimate_zero_point is True:
        zero_point = zero_point_est
    print("Using:                {}".format(zero_point))
    print()

    # Apply calibration
    print("Calculating vectors... ")
    acc_c = tools.apply_calibration(acc, acc_scale, acc_offset)
    mag_c = tools.apply_calibration(mag, mag_scale, mag_offset)
    zero_point_c = tools.apply_calibration(zero_point, acc_scale, acc_offset)
    tools.set_vector_in_dataframe(data, 'acc', acc)

    # Plot sensor readings before and after calibration
    plots.plot_acc_mag_vectors(data.index, acc, mag, os.path.join(output_dir, "acc_mag_raw.pdf"))
    plots.plot_acc_mag_vectors(data.index, acc_c, mag_c, os.path.join(output_dir, "acc_mag_calibrated.pdf"))

    # Calculate tilt vectors, temperature and battery
    up_proj, east_proj, north_proj = tools.calculate_tilt_vector(acc_c, mag_c, zero_point_c)
    data.temp = tools.calculate_temperature(data.temp.values, serial)
    data.batt = tools.calculate_battery(data.batt.values)
    plots.plot_temperature(data, os.path.join(output_dir, "temperature.pdf"))
    plots.plot_battery(data, os.path.join(output_dir, "battery.pdf"))
    data['tilt_z'] = up_proj
    data['tilt_x'] = east_proj
    data['tilt_y'] = north_proj
    print("done")
    print()
    # Smooth and resample
    if resample is None:
        window = input("Enter the resampling period in seconds: ")
    else:
        window = resample
    print("Resampling interval: {}s ({} samples)".format(interval, int(window / interval)))
    print()
    window = int(window / interval)

    print("Calculating resampled values (this may take a while)... ")
    data_sm = data[['tilt_x', 'tilt_y', 'tilt_z', 'batt', 'temp']] \
        .rolling(int(window * 3), min_periods=1, win_type='gaussian', center=True) \
        .mean(std=window / 3) \
        .resample('600s') \
        .first()
    print("done")
    print()

    # Normalise after resampling
    tilt = tools.get_vector_from_dataframe(data_sm, 'tilt')
    tilt = tilt / np.linalg.norm(tilt, axis=1)[:, np.newaxis]
    tools.set_vector_in_dataframe(data_sm, 'tilt', tilt)

    # Calculate tilt angle, direction and heading
    data_sm['tilt'] = np.arccos(data_sm.tilt_z)
    data_sm['dir'] = np.arctan2(data_sm.tilt_y, data_sm.tilt_x)
    data_sm['heading'] = np.mod(90 - data_sm.dir * 180 / np.pi, 360)

    plots.plot_tilt(data_sm, os.path.join(output_dir, "tilt.pdf"))
    plots.plot_heading(data_sm, os.path.join(output_dir, "heading.pdf"))

    # Calculate speed and error
    data_sm['speed'], data_sm['speed_error'] = tools.calculate_speed(data_sm.tilt.values)
    plots.plot_speed(data_sm, os.path.join(output_dir, "speed.pdf"))

    # Save to CSV
    output = os.path.join(output_dir, os.path.basename(source)[:-4] + ".csv")
    print("Saving to CSV: {}".format(output))
    data_sm.to_csv(output)

    # Show plots
    if show_plots:
        plt.show()
    else:
        plt.close("all")

    return data, data_sm


if __name__ == '__main__':
    source = r"D:\Dropbox Backup\JCU\CurrentMeter\Magnarotte\Deployments\2015-05-21 - Moore Reef - All\cm\14-A0027007.CSV"
    # data, data_sm = process(source,
    #                         date_range=['2015-07-14 00:00:00', '2015-12-06 00:00:00'],
    #                         estimate_zero_point=True,
    #                         smoothing_window=600,
    #                         name="test_small")
    data, data_sm = process(source,
                            date_range=None,
                            estimate_date_range=False,
                            estimate_zero_point=True,
                            smoothing_window=600,
                            output_name="test",
                            show_plots=True)

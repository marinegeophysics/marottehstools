import pandas as pd
import csv
import numpy as np
import matplotlib.pyplot as plt
import datetime
import importlib.resources as pkg_resources
import marottehs.plots as plots


def read_record(source):
    return pd.read_csv(source, header=9, parse_dates=True, index_col=0, date_parser=parse_datetime)


def read_calibration(source, header_lines=9):
    with open(source) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        serial = None
        name = None
        firmware = None
        zero_point = None
        acc_scale = None
        acc_offset = None
        mag_scale = None
        mag_offset = None

        row_count = 0
        for row in csv_reader:
            if row[0] == "serial":
                serial = row[1]
            elif row[0] == "name":
                name = row[1]
            elif row[0] == "firmware":
                firmware = row[1]
            elif row[0] == "zeropoint":
                zero_point = np.asarray((float(row[1]), float(row[2]), float(row[3])))
            elif row[0] == "acccal":
                acc_scale = np.asarray((float(row[1]), float(row[2]), float(row[3])))
                acc_offset = np.asarray((float(row[4]), float(row[5]), float(row[6])))
            elif row[0] == "magcal":
                mag_scale = np.asarray((float(row[1]), float(row[2]), float(row[3])))
                mag_offset = np.asarray((float(row[4]), float(row[5]), float(row[6])))

            row_count += 1
            if row_count >= header_lines:
                break
    return serial, name, firmware, zero_point, acc_scale, acc_offset, mag_scale, mag_offset


def parse_datetime(d):
    return datetime.datetime.strptime(d, '%Y-%m-%d %H:%M:%S.%f')


def get_vector_from_dataframe(data, name):
    vector = data[[name + '_x', name + '_y', name + '_z']].values
    return vector


def set_vector_in_dataframe(data, name, vector):
    data[name + '_x'] = vector[:, 0]
    data[name + '_y'] = vector[:, 1]
    data[name + '_z'] = vector[:, 2]
    return vector


def apply_calibration(vector, scale, offset):
    vector = (vector - offset) / scale
    return vector


def deviation(vector):
    diff = vector[1:, :] - vector[:-1, :]
    dev = np.linalg.norm(diff, axis=1)
    dev = np.append(dev, 1)
    return dev


def estimate_zero_point(acc, deviation_threshold, up_threshold):
    dev = deviation(acc)
    bins = np.arange(-5000, 5000)
    p = np.asarray(range(dev.shape[0]))
    # Data points with low deviation (current meter not moving) and instrument pointing up (high up reading) are valid points
    idx = np.logical_and(dev < deviation_threshold, np.abs(acc[:, 1].flatten()) > up_threshold)
    # plots.plot_vector(p[idx], acc[idx,:])
    # print(p[idx].shape)
    # plt.figure(figsize=(16,8))
    # plt.plot(data_z.index[:-1], diff_z), plt.show()
    offset = np.zeros(3)
    for i in range(3):
        hist = np.histogram(acc[idx, i], bins=bins)
        offset[i] = bins[np.argmax(hist[0])]
    return offset


def estimate_date_range(data, threshold):
    # Get the daily mean of the up value
    data_up = data['acc_y'].resample('1D').mean()
    # Difference between each day
    diff_up = np.abs(np.diff(data_up))
    # Find days where the difference was large
    arg_up = np.argwhere(diff_up > threshold).flatten()
    # Valid dates are the middle period between these
    start = arg_up[arg_up < len(diff_up) / 2][-1] + 1
    end = arg_up[arg_up > len(diff_up) / 2][0]
    start_date = data_up.index[start]
    end_date = data_up.index[end]
    # plt.figure(figsize=(16,8))
    # plt.plot(data_z.index[:-1], diff_z), plt.show()
    return [start_date, end_date]


def calculate_tilt_vector(acc, mag, zero_point):
    # Normalise
    acc = acc / np.linalg.norm(acc, axis=1)[:, np.newaxis]
    mag = mag / np.linalg.norm(mag, axis=1)[:, np.newaxis]
    zero_point = zero_point / np.linalg.norm(zero_point)

    # Create world axes
    up = acc
    east = np.cross(mag, up)
    east = east / np.linalg.norm(east, axis=1)[:, np.newaxis]
    north = np.cross(up, east)
    north = north / np.linalg.norm(north, axis=1)[:, np.newaxis]

    # Project zero point onto axes
    up_proj = np.sum(zero_point * up, axis=1)
    east_proj = np.sum(zero_point * east, axis=1)
    north_proj = np.sum(zero_point * north, axis=1)

    return up_proj, east_proj, north_proj


def calculate_speed(tilt):
    csv_file = pkg_resources.open_text('marottehs', 'calibration.csv')
    speed_calibration = pd.read_csv(csv_file)
    speed = np.interp(tilt,
                      speed_calibration.tilt.values,
                      speed_calibration.speed.values)
    speed_error = np.interp(tilt,
                            speed_calibration.tilt.values,
                            speed_calibration.sys_error.values)
    return speed, speed_error


def calculate_temperature(temp, serial):
    # A version boards and the initial B0001 - B0050 boards with the same thermistor
    A_B0 = [1.129241E-03, 2.341077E-04, 8.775468E-08, 0]
    # All other B boards
    B1 = [1.13929600457259E-03, 2.31949467390149E-04, 1.05992476218967E-07, -6.67898975192618E-11]

    if serial.startswith("A") or serial.startswith("B0"):
        cal = A_B0
    else:
        cal = B1

    temp = 10000.0 / (1023.0 / temp - 1)
    temp = 1.0 / (cal[0] + cal[1] * np.log(temp) + cal[2] * np.power(np.log(temp), 3) + cal[3] * np.power(np.log(temp), 5)) - 273.15
    return temp


def calculate_battery(batt):
    return batt / 1000

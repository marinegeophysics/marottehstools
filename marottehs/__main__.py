import sys
import argparse
import marottehs.processing as mhs


def main(args):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Marotte HS Current Meter Processing',
                                     epilog='Example of script with fixed dates and 5 minute resampling:\n'
                                            'python -m marottehs -i "C:\\Users\\Deployments\\record.txt" '
                                            '-s "2019-01-01 00:00:00" -e "2019-01-01 00:00:00" -p -r 300\n\n'
                                            'Example of script with estimated dates, estimated zero point and 10 minute resampling:\n'
                                            'python -m marottehs -i "C:\\Users\\Deployments\\record.txt" -z -d -p -r 600\n\n'
                                            'If speeds seem offset, try to use zero point estimation if applicable, as the results are usually more accurate',
                                     formatter_class=argparse.RawTextHelpFormatter)
    # Source
    parser.add_argument('-i',
                        '--input',
                        dest='source',
                        action='store',
                        type=str,
                        default=None,
                        help='Current meter record to process')
    # Start date
    parser.add_argument('-s',
                        '--start-date',
                        dest='start',
                        action='store',
                        type=str,
                        default=None,
                        help='Start date of deployment written as \"YYYY-MM-DD hh:mm:ss\" (include quote marks)')
    # End date
    parser.add_argument('-e',
                        '--end-date',
                        dest='end',
                        action='store',
                        type=str,
                        default=None,
                        help='Start date of deployment written as \"YYYY-MM-DD hh:mm:ss\" (include quote marks)')
    # Smoothing window
    parser.add_argument('-r',
                        '--resample',
                        dest='resample',
                        action='store',
                        type=int,
                        default=600,
                        help='Time period to smooth and resample in seconds (default 600)')
    # Name
    parser.add_argument('-n',
                        '--name',
                        dest='name',
                        action='store',
                        type=str,
                        default=None,
                        help='Name to append to the output director, e.g. \"site_15\"')
    # Estimate zero point
    parser.add_argument('-z',
                        '--est-zero',
                        dest='est_zero',
                        action='store_const',
                        const=True,
                        default=False,
                        help='Estimate the zero point from the deployment. '
                             'Only use if deployed at site where currents occasionally approach 0, e.g. open ocean tidal zones.')
    # Estimate zero point
    parser.add_argument('-d',
                        '--est-dates',
                        dest='est_dates',
                        action='store_const',
                        const=True,
                        default=False,
                        help='Estimate the valid deployed dates. Use instead of start and end dates')
    # Estimate zero point
    parser.add_argument('-p',
                        '--show-plots',
                        dest='show_plots',
                        action='store_const',
                        const=True,
                        default=False,
                        help='Show the plots at the end of processing')

    parsed_args = parser.parse_args(args)
    if parsed_args.start is None or parsed_args.end is None:
        date_range = None
    else:
        date_range = [parsed_args.start, parsed_args.end]
    mhs.process(parsed_args.source,
                date_range=date_range,
                estimate_date_range=parsed_args.est_dates,
                estimate_zero_point=parsed_args.est_zero,
                resample=parsed_args.resample,
                output_name=parsed_args.name,
                show_plots=parsed_args.show_plots)


if __name__ == '__main__':
    main(sys.argv[1:])

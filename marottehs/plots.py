import numpy as np
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
import os

register_matplotlib_converters()


def plot_acc_mag_vectors(datetime, acc, mag, save_file=None):
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(16, 8))
    ax1.plot(datetime, acc[:, 0])
    ax1.plot(datetime, acc[:, 1])
    ax1.plot(datetime, acc[:, 2])
    lim = np.max(np.abs(ax1.get_ylim())) * 1.1
    ax1.set_ylim((-lim, lim))
    ax1.set_title("Accelerometer")
    ax1.grid("on")
    ax1.legend(('x', 'y', 'z'), loc='upper right')
    ax2.plot(datetime, acc[:, 0])
    ax2.plot(datetime, mag[:, 1])
    ax2.plot(datetime, mag[:, 2])
    lim = np.max(np.abs(ax2.get_ylim())) * 1.1
    ax2.set_ylim((-lim, lim))
    ax2.set_title("Magnetometer")
    ax2.grid("on")
    ax2.legend(('x', 'y', 'z'), loc='upper right')
    show_and_save(fig, save_file)


def plot_vector(datetime, vector, save_file=None):
    fig = plt.figure(figsize=(16, 8))
    plt.plot(datetime, vector[:, 0])
    plt.plot(datetime, vector[:, 1])
    plt.plot(datetime, vector[:, 2])
    lim = np.max(np.abs(plt.ylim())) * 1.1
    plt.ylim((-lim, lim))
    plt.title("Vector")
    plt.grid("on")
    plt.legend(('x', 'y', 'z'), loc='upper right')
    show_and_save(fig, save_file)


def plot_tilt(data, save_file=None):
    fig = plt.figure(figsize=(16, 8))
    plt.plot(data.tilt)
    plt.grid("on")
    plt.ylim(0, 1.5)
    plt.xlim(data.index[0], data.index[-1])
    plt.title("Tilt")
    plt.xlabel("Date")
    plt.ylabel("Tilt angle (rad)")
    show_and_save(fig, save_file)


def plot_heading(data, save_file=None):
    fig = plt.figure(figsize=(16, 8))
    plt.scatter(data.index, data.heading, data.tilt * 10)
    plt.grid("on")
    plt.ylim(0, 360)
    plt.xlim(data.index[0], data.index[-1])
    plt.title("Heading")
    plt.xlabel("Date")
    plt.ylabel("Heading (deg CW from North)")
    show_and_save(fig, save_file)


def plot_speed(data, save_file=None):
    grey = [0.8, 0.8, 0.8]
    fig = plt.figure(figsize=(16, 8))
    plt.plot(data.speed + data.speed_error, color=grey)
    plt.plot(data.speed - data.speed_error, color=grey)
    plt.plot(data.speed)
    plt.grid("on")
    plt.ylim(0, 1)
    plt.xlim(data.index[0], data.index[-1])
    plt.title("Speed")
    plt.xlabel("Date")
    plt.ylabel("Speed (m/s)")
    show_and_save(fig, save_file)


def plot_temperature(data, save_file=None):
    fig = plt.figure(figsize=(16, 8))
    plt.plot(data.temp)
    plt.grid("on")
    plt.ylim(0, 40)
    plt.xlim(data.index[0], data.index[-1])
    plt.title("Temperature")
    plt.xlabel("Date")
    plt.ylabel("Temperature (deg)")
    show_and_save(fig, save_file)


def plot_battery(data, save_file=None):
    fig = plt.figure(figsize=(16, 8))
    plt.plot(data.batt)
    plt.grid("on")
    plt.ylim(0, 4)
    plt.xlim(data.index[0], data.index[-1])
    plt.title("Battery Voltage")
    plt.xlabel("Date")
    plt.ylabel("Voltage (V)")
    show_and_save(fig, save_file)


def show_and_save(fig, save_file=None):
    if save_file is not None:
        os.makedirs(os.path.dirname(save_file), exist_ok=True)
        fig.savefig(save_file)
    plt.draw()
